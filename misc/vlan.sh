#!/bin/bash

# VLAN config commands for HVAC networks
# Network interface to use (e.g. eth0, eth1, ...)
interface=eth1


# HVAC system 
vlanid3=3
interfacevlanid3=${interface}.${vlanid3}
ip link add link $interface name $interfacevlanid3 type vlan id $vlanid3
ip addr add 172.17.15.9/24 brd 172.17.15.255 dev $interfacevlanid3
ip link set dev $interfacevlanid3 up

